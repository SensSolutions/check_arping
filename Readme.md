# check_arping

Nagios plugin to  arpings a host.
Checks if host is alive and round trip time compared with warning and critical
times specified as parameters.

The exit code are:

    0:  all got well
    1:  connection is slow
    2:  no connection at all (or really slow)
    3:  something really bad happend

and the round trip time.

There's 2 versions, for Python2 and Python3.

Verified on Linux and macos X

On Linux you **must** install arping package.
pe on Ubuntu Bionic:

```
Package: arping
Version: 2.19-4
Priority: optional
Section: universe/net
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Salvatore Bonaccorso <carnil@debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 73,7 kB
Depends: libc6 (>= 2.15), libnet1 (>= 1.1.2.1), libpcap0.8 (>= 1.5.1)
Conflicts: iputils-arping
Homepage: http://www.habets.pp.se/synscan/programs.php?prog=arping
Download-Size: 28,3 kB
APT-Manual-Installed: yes
APT-Sources: http://es.archive.ubuntu.com/ubuntu bionic/universe amd64 Packages
Description: sends IP and/or ARP pings (to the MAC address)
 The arping utility sends ARP and/or ICMP requests to the specified host
 and displays the replies. The host may be specified by its hostname,
 its IP address, or its MAC address.
```


## Usage

Check python and arping paths:

```python
DEBUG = False
# Linux
ARPING = "/usr/sbin/arping"
```


```bash
root@ubu1804:~# ./check_arping3.py -h
usage: check_arping3.py [-h] -H <IP or MAC> [-I <interface>] [-W <msec>] 
                        [-C <msec>]

Check with arping if HOST is alive.

optional arguments:
  -h, --help      show this help message and exit
  -H <IP or MAC>  the HOST to check
  -I <interface>  the interface to use (Default: ens33)
  -W <msec>       the WARNING time (Default: 2ms)
  -C <msec>       the CRITICAL time (Default: 5ms)
```

For a wire network, warning can be ≥20ms and critical ≥ 60ms.
For a wireless one warning ≥50ms and without backhauls critical ≥100ms.

```bash
root@ubu1804:~# check_arping3.py -H 9c:8e:99:c4:65:d8\
                                 -I ens33 -W 50 -C 100
33.7271
```
Check the exit code:

```bash
root@ubu1804:~# echo $?
0
```
## Miscelaneous

Command to create this PDF:

```bash
pandoc check_arping.md \
    --pdf-engine=xelatex\
    --variable mainfont="Times"\
    --variable monofont="Courier"\
    --variable fontsize=12pt\
    --highlight-style tango\
    --toc\
    -o check_arping.pdf
```
